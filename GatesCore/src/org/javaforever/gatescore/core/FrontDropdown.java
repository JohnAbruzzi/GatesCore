package org.javaforever.gatescore.core;

import java.io.Serializable;

import org.javaforever.gatescore.exception.ValidateException;
import org.javaforever.gatescore.utils.StringUtil;

public class FrontDropdown extends FrontField implements Comparable<FrontField> ,Cloneable,Serializable {
	private static final long serialVersionUID = -3255678030636878579L;
	protected String aliasName;
	protected String targetName;
	protected FrontDomain target;
	
	public String getFieldComment() {
		return fieldComment;
	}

	public void setFieldComment(String fieldComment) {
		this.fieldComment = fieldComment;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public FrontDropdown(String targetName){
		super();
		this.targetName = targetName;
	}
	
	public void decorate(FrontDomain domain) throws ValidateException{
		if (StringUtil.isBlank(domain)) throw new ValidateException("目标域对象设置错误！","error");
		else if (StringUtil.isBlank(this.targetName)) throw new ValidateException("未设置目标域对象名称！","error");
		this.target = domain;
		if (StringUtil.isBlank(this.aliasName)) {
			this.aliasName = this.target.getLowerFirstDomainName() +"Id";
		}		
		FrontField f = (FrontField)domain.getDomainId().clone();
		f.setFieldName(this.aliasName);
		this.setFieldName(f.getFieldName());
		this.setFieldComment(f.getFieldComment());
		this.setFieldValue(f.getFieldValue());
		//this.setLabel(f.getLabel());
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public FrontDomain getTarget() {
		return target;
	}

	public void setTarget(FrontDomain target) {
		this.target = target;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	
	public String getForeignKeyAlias(){
		return this.target.getLowerFirstDomainName()+"Id";		
	}
}
