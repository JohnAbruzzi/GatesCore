package org.javaforever.gatescore.core;

import java.io.Serializable;

import org.javaforever.gatescore.utils.StringUtil;

public class FrontManyToMany implements Comparable<FrontManyToMany>,Cloneable,Serializable{
	private static final long serialVersionUID = -2150939874821338634L;
	protected FrontDomain master;
	protected FrontDomain slave;
	protected String standardName;
	protected String label;
	protected String values;
	protected String manyToManyMasterName;
	protected String manyToManySalveName;
	protected String masterValue;
	protected String title = "";
	protected String subTitle = "";
	protected String footer = "";
	protected String slaveAlias = "";
	protected String slaveAliasLabel;
	
	public FrontManyToMany(String manyToManyMasterName,String manyToManySalveName){
		super();
		this.manyToManyMasterName = manyToManyMasterName;
		this.manyToManySalveName = manyToManySalveName;
	}	
	
	public FrontManyToMany(FrontDomain master,FrontDomain slave,String masterValue, String values){
		this.master = master;
		this.slave = slave;
		this.masterValue = masterValue;
		this.values = values;
	}
	
	public FrontManyToMany(FrontDomain master,FrontDomain slave,String masterValue, String values, String slaveAlias){
		this.master = master;
		this.setManyToManyMasterName(master.getStandardName());
		this.slave = slave;
		this.slave.setAlias(slaveAlias);
		this.masterValue = masterValue;
		this.values = values;
		this.slaveAlias = slaveAlias;
	}
	
	public void setMaster(FrontDomain master) {
		this.master = master;
	}
	public FrontDomain getSlave() {
		return slave;
	}
	public void setSlave(FrontDomain slave) {
		this.slave = slave;
	}
	
	public FrontDomain getMaster() {
		return master;
	}
	public String getStandardName(){
		if (this.master !=null && this.slave !=null){
			if (StringUtil.isBlank(this.slave.getAlias())){
				return "Link"+this.master.getStandardName()+this.slave.getStandardName();
			}else {
				return "Link"+this.master.getStandardName()+this.slave.getAlias();
			}
		} else {
			return this.standardName;
		}
	}
	@Override
	public int compareTo(FrontManyToMany o) {
		if (!StringUtil.isBlank(this.standardName)&&!StringUtil.isBlank(o.getStandardName())) {
			return this.getStandardName().compareTo(o.getStandardName());
		} else {
			if (this.master!=null && this.slave != null && o.getMaster()!=null && o.getSlave()!=null) {
				int result = this.master.compareTo(o.getMaster());
				if (result == 0) {
					result = this.slave.compareTo(o.getSlave());
				}
				return result;
			}else {
				int result = this.manyToManyMasterName.compareTo(o.getManyToManyMasterName());
				if (result == 0) {
					String slaveAliasOrName = StringUtil.isBlank(this.slaveAlias)?this.manyToManySalveName:this.slaveAlias;
					String oAliasOrName = StringUtil.isBlank(o.getSlaveAlias())?o.getManyToManySalveName():o.getSlaveAlias();
					result = slaveAliasOrName.compareTo(oAliasOrName);
				}
				return result;
			}
		}
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getText(){
		if (!StringUtil.isBlank(this.getSlaveAliasLabelOrText())) {
			if (StringUtil.isEnglishAndDigitalAndEmpty(this.getSlaveAliasLabelOrText())) {
				return "Link "+this.master.getStandardName()+" "+this.getSlaveAliasLabelOrText();
			}else {
				return "链接"+this.master.getText()+this.getSlaveAliasLabelOrText();
			}
		}
		if (StringUtil.isBlank(this.master.getLabel())||StringUtil.isEnglishAndDigitalAndEmpty(this.master.getLabel())){
			if (StringUtil.isBlank(this.slave.getAlias())) {
				return "Link "+this.master.getStandardName()+" "+this.slave.getStandardName();
			} else {
				return "Link "+this.master.getStandardName()+" "+this.slave.getAlias();
			}
		}
		else return "链接"+this.master.getText()+this.slave.getText();
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getManyToManySalveName() {
		return manyToManySalveName;
	}

	public void setManyToManySalveName(String manyToManySalveName) {
		this.manyToManySalveName = manyToManySalveName;
	}

	public String getMasterValue() {
		return masterValue;
	}

	public void setMasterValue(String masterValue) {
		this.masterValue = masterValue;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getSlaveAlias() {
		if (!StringUtil.isBlank(this.slaveAlias)) {
			return this.slaveAlias;
		} else {
			return this.manyToManySalveName;
		}
	}

	public void setSlaveAlias(String slaveAlias) {
		this.slaveAlias = slaveAlias;
	}

	public String getManyToManyMasterName() {
		return manyToManyMasterName;
	}

	public void setManyToManyMasterName(String manyToManyMasterName) {
		this.manyToManyMasterName = manyToManyMasterName;
	}

	public String getSlaveAliasLabel() {
		return slaveAliasLabel;
	}

	public void setSlaveAliasLabel(String slaveAliasLabel) {
		this.slaveAliasLabel = slaveAliasLabel;
	}

	public String getSlaveAliasLabelOrText() {
		if (!StringUtil.isBlank(this.slaveAliasLabel)) return this.getSlaveAliasLabel();
		else return this.slave.getText();
	}
}
