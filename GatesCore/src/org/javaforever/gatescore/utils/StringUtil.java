package org.javaforever.gatescore.utils;

public class StringUtil {
	public static String capFirst(String value){
		if (value!=null && !value.equals("")) return value.substring(0, 1).toUpperCase()+value.substring(1);
		else return "";
	}

	public static String lowerFirst(String value){
		if (value!=null && !value.equals("")) return value.substring(0, 1).toLowerCase()+value.substring(1);
		else return "";	
	}
	
	public static String changeDomainFieldtoTableColum(String value){
		if (StringUtil.isBlank(value)) return "";
		StringBuilder sb = new StringBuilder(value);
		StringBuilder sb0 = new StringBuilder("");
		boolean continueCap = false;
		for(int i=0; i < sb.length(); i++){
			char ch = sb.charAt(i);
			if (ch<='Z'&& ch>='A'&&i>0&&!continueCap){
				sb0.append("_").append((""+ch).toLowerCase());
				continueCap = true;
			}else if (ch<='Z'&& ch>='A'&&i==0){
				sb0.append((""+ch).toLowerCase());
				continueCap = true;
			} else if (ch<='Z'&& ch>='A'&&continueCap){
				sb0.append((""+ch).toLowerCase());
			}else if (ch<='z'&& ch>='a') {
				sb0.append(ch);
				continueCap = false;
			}else {
				sb0.append(ch);
			}
		}
		return sb0.toString();
	}
	
	public static boolean isBlank(Object o){
		if (o==null || "".equals(o)) return true;
		else return false;
	}
	
	public static boolean isEnglishAndDigitalAndEmpty(String value) {
		for (int i=0;i<value.length();i++) {
			if (!isEnglishOrDigitalOrEmpty(value.charAt(i))) return false;
		}
		return true;
	}
	
	public static boolean isEnglishOrDigitalOrEmpty(char c) {
		if ((c>='a'&&c<='z')||(c>='A'&&c<='Z')||(c>='0'&&c<='9')||c==' '||c=='\t'||c=='\n') return true;
		else return false;
	}
	
	public static String filterSingleQuote(String value){
		return value.replace("'", "");
	}
	
	public static boolean isLowerCaseLetter(String value){
		if (isBlank(value)) return false;
		char c = value.charAt(0);
		if (c>='a'&&c<='z') return true;
		else return false;
	}
	
	public static boolean isLowerCaseLetterPosition(String value,int position){
		if (isBlank(value)) return false;
		char c = value.charAt(position);
		if (c>='a'&&c<='z') return true;
		else return false;
	}
	
	public static boolean isBoolean(String value) {
		if ("true".equals(value)||"false".equals(value)) return true;
		else return false;
	}
}
