package org.javaforever.gatescore.verb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.utils.StringUtil;

public class SearchByFieldsByPage extends FrontVerb {
	private static final long serialVersionUID = -6170458555312552595L;

	public SearchByFieldsByPage(FrontDomain d) {
		super(d);	
		this.setVerbName("SearchByFieldsByPage"+d.getCapFirstDomainName());
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("search"+StringUtil.capFirst(this.domain.getPlural())+"ByFieldsByPage");
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage() {"));
		sList.add(new Statement(2000L,1,"this.listLoading = true"));
		sList.add(new Statement(3000L,1,"search"+this.domain.getCapFirstPlural()+"ByFieldsByPage(this.buildParams(this.listQuery, this.postForm)).then("));
		sList.add(new Statement(4000L,2,"response => {"));
		sList.add(new Statement(5000L,2,"this.list = response.data.rows"));
		sList.add(new Statement(6000L,2,"this.total = response.data.total"));
		sList.add(new Statement(7000L,2,"this.listLoading = false"));
		sList.add(new Statement(8000L,1,"})"));
		sList.add(new Statement(9000L,1,"},"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("search"+StringUtil.capFirst(this.domain.getPlural())+"ByFieldsByPage");
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"export function search"+this.domain.getCapFirstPlural()+"ByFieldsByPage(data) {"));
		sList.add(new Statement(2000L,0,"console.log(data);"));
		sList.add(new Statement(3000L,0,"return request({"));
		if (this.domain.isUseController()) {
			sList.add(new Statement(4000L,1,"url: '"+this.domain.getLowerFirstDomainName()+"Controller/search"+this.domain.getCapFirstPlural()+"ByFieldsByPage',"));
		} else {
			sList.add(new Statement(4000L,1,"url: '"+this.domain.getLowerFirstDomainName()+"Facade/search"+this.domain.getCapFirstPlural()+"ByFieldsByPage',"));
		}
		sList.add(new Statement(5000L,1,"method: 'post',"));
		sList.add(new Statement(6000L,1,"params:data"));
		sList.add(new Statement(7000L,0,"})"));
		sList.add(new Statement(8000L,0,"}"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		FrontCodeBlock codeBlock = new FrontCodeBlock();
		List<Writeable> sList = new ArrayList<Writeable>();
		codeBlock.setMethodStatementList(WriteableUtil.merge(sList));
		return codeBlock;
	}


}
