package org.javaforever.gatescore.verb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;

public class ListAllByPage extends FrontVerb{
	private static final long serialVersionUID = -5590619489251226554L;

	public ListAllByPage(FrontDomain d) {
		super(d);
		this.setVerbName("ListAllByPage"+d.getCapFirstDomainName());
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
