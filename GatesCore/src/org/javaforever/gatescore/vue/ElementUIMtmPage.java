package org.javaforever.gatescore.vue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.complexverb.Assign;
import org.javaforever.gatescore.complexverb.ListMyActive;
import org.javaforever.gatescore.complexverb.ListMyAvailableActive;
import org.javaforever.gatescore.complexverb.Revoke;
import org.javaforever.gatescore.core.FrontManyToMany;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.StatementList;
import org.javaforever.gatescore.core.UtilsMethodGenerator;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;
import org.javaforever.gatescore.utils.StringUtil;
import org.javaforever.gatescore.verb.ListActive;

public class ElementUIMtmPage extends ElementUIPage implements Comparable<ElementUIMtmPage>,Cloneable,Serializable{
	private static final long serialVersionUID = 551411229367131296L;
	protected FrontManyToMany mtm;
	protected Long serial = 0L;
	protected String standardName;
	
	public ElementUIMtmPage (FrontManyToMany mtm) {
		super();
		this.mtm = mtm;
	}

	@Override
	public int compareTo(ElementUIMtmPage o) {
		return this.standardName.compareTo(o.getStandardName());
	}

	public FrontManyToMany getMtm() {
		return mtm;
	}

	public void setMtm(FrontManyToMany mtm) {
		this.mtm = mtm;
	}

	public Long getSerial() {
		return serial;
	}

	public void setSerial(Long serial) {
		this.serial = serial;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	@Override
	public StatementList generateStatementList() throws Exception{
		Assign assign = new Assign(this.mtm);
		Revoke revoke = new Revoke(this.mtm);
		ListMyActive lma = new ListMyActive(this.mtm);
		ListMyAvailableActive lmaa = new ListMyAvailableActive(this.mtm);
		ListActive mla = new ListActive(this.mtm.getMaster());
		ListActive sla = new ListActive(this.mtm.getSlave());		
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"<style>"));
		sList.add(new Statement(2000L,1,".el-transfer-panel{"));
		sList.add(new Statement(3000L,1,"height:700px;"));
		sList.add(new Statement(4000L,1,"}"));
		sList.add(new Statement(5000L,1,".el-transfer-panel__list{"));
		sList.add(new Statement(6000L,1,"height: 650px;"));
		sList.add(new Statement(7000L,1,"}"));
		sList.add(new Statement(8000L,0,"</style>"));
		sList.add(new Statement(9000L,0,"<template>"));
		sList.add(new Statement(10000L,0,"<div class=\"app-container\">"));
		sList.add(new Statement(11000L,0,"<el-row>"));
		sList.add(new Statement(12000L,1,"<el-col :span=\"4\">"));
		sList.add(new Statement(13000L,1,"<el-card class=\"box-card\"　:height=\"700\">"));
		sList.add(new Statement(14000L,2,"<div slot=\"header\" class=\"clearfix\">"));
		sList.add(new Statement(15000L,2,"<span>活跃"+this.mtm.getMaster().getText()+"</span>"));
		sList.add(new Statement(16000L,2,"</div>"));
		sList.add(new Statement(17000L,2,"<el-radio-group v-model=\""+this.mtm.getMaster().getLowerFirstDomainName()+"Id\">"));
		sList.add(new Statement(18000L,2,"<div v-for=\""+this.mtm.getMaster().getLowerFirstDomainName()+" in this.active"+this.mtm.getMaster().getCapFirstPlural()+"\" :key=\""+this.mtm.getMaster().getLowerFirstDomainName()+"."+this.mtm.getMaster().getDomainId().getLowerFirstFieldName()+"\" class=\"text item\">"));
		sList.add(new Statement(19000L,3,"<el-button type=\"text\"><el-radio :label=\""+this.mtm.getMaster().getLowerFirstDomainName()+".id\" name=\""+this.mtm.getMaster().getLowerFirstDomainName()+"Id\" @change=\"listAvailableActive"+this.mtm.getMaster().getCapFirstDomainName()+""+this.mtm.getSlave().getAliasOrNamePlural()+"Using"+this.mtm.getMaster().getCapFirstDomainName()+"Id("+this.mtm.getMaster().getLowerFirstDomainName()+"."+this.mtm.getMaster().getDomainId().getLowerFirstFieldName()+")\">{{"+this.mtm.getMaster().getLowerFirstDomainName()+"."+this.mtm.getMaster().getDomainName().getLowerFirstFieldName()+" }}</el-radio></el-button>"));
		sList.add(new Statement(20000L,2,"</div>"));
		sList.add(new Statement(21000L,2,"</el-radio-group>"));
		sList.add(new Statement(22000L,1,"</el-card>"));
		sList.add(new Statement(23000L,1,"</el-col>"));
		sList.add(new Statement(24000L,1,"<el-col :span=\"12\">"));
		sList.add(new Statement(25000L,2,"<el-transfer v-model=\"this.availableActive"+this.mtm.getSlave().getAliasOrNamePlural()+"\"　:props=\"{key: 'id',label: '"+StringUtil.lowerFirst(this.mtm.getSlave().getDomainName().getFieldName())+"'}\" :data=\"this.activeAll"+this.mtm.getSlave().getAliasOrNamePlural()+"\" :titles=\"this.transHeaders\" @change=\"handleChange\"></el-transfer>"));
		sList.add(new Statement(26000L,1,"</el-col>"));
		sList.add(new Statement(27000L,0,"</el-row>"));
		sList.add(new Statement(28000L,0,"</div>"));
		sList.add(new Statement(29000L,0,"</template>"));
		sList.add(new Statement(30000L,0,"<script>"));
		sList.add(new Statement(31000L,0,"import {"));
		sList.add(new Statement(32000L,0,mla.generateApiMethod().getLowerFirstMethodName() + ","+lma.generateApiMethod().getLowerFirstMethodName()+","+lmaa.generateApiMethod().getLowerFirstMethodName()+","+assign.generateApiMethod().getLowerFirstMethodName()+","+revoke.generateApiMethod().getLowerFirstMethodName()));
		sList.add(new Statement(33000L,0,"} from '@/api/"+this.mtm.getMaster().getPlural().toLowerCase()+"'"));
		sList.add(new Statement(34000L,0,""));
		sList.add(new Statement(35000L,0,"import {"));
		sList.add(new Statement(36000L,0,sla.generateApiMethod().getLowerFirstMethodName()));
		sList.add(new Statement(37000L,0,"} from '@/api/"+this.mtm.getSlave().getPlural().toLowerCase()+"'"));
		sList.add(new Statement(38000L,0,""));
		sList.add(new Statement(39000L,0,"export function isBlank(val) {"));
		sList.add(new Statement(40000L,0,"if (val == undefined || val == null || val == \"\" || val == \"0\" || val == 0) return true;"));
		sList.add(new Statement(41000L,0,"else return false;"));
		sList.add(new Statement(42000L,0,"}"));
		sList.add(new Statement(43000L,0,""));
		sList.add(new Statement(44000L,0,"export default {"));
		sList.add(new Statement(45000L,0,"inject:['reload'],"));
		sList.add(new Statement(46000L,0,"name: 'link"+this.mtm.getMaster().getCapFirstDomainName()+""+StringUtil.capFirst(this.mtm.getSlave().getAliasOrName())+"',"));
		sList.add(new Statement(47000L,0,"filters: {"));
		sList.add(new Statement(48000L,0,"},"));
		sList.add(new Statement(49000L,0,"data() {"));
		sList.add(new Statement(50000L,1,"return {"));
		sList.add(new Statement(51000L,2,""+this.mtm.getMaster().getLowerFirstDomainName()+"Id:1,"));
		sList.add(new Statement(52000L,2,"active"+this.mtm.getMaster().getCapFirstPlural()+":[],"));
		sList.add(new Statement(53000L,2,"activeAll"+this.mtm.getSlave().getAliasOrNamePlural()+":[],"));
		sList.add(new Statement(54000L,2,"availableActive"+this.mtm.getSlave().getAliasOrNamePlural()+":[],"));
		sList.add(new Statement(55000L,2,"transHeaders:['已有"+this.mtm.getSlave().getText()+"','可添加"+this.mtm.getSlave().getText()+"'],"));
		sList.add(new Statement(56000L,1,"}"));
		sList.add(new Statement(57000L,0,"},"));
		sList.add(new Statement(58000L,0,"created() {"));
		sList.add(new Statement(59000L,1,"this.listActive"+this.mtm.getMaster().getCapFirstPlural()+"();"));
		sList.add(new Statement(60000L,1,"this.listAllActive"+this.mtm.getSlave().getAliasOrNamePlural()+"();"));
		sList.add(new Statement(61000L,1,"this.listAvailableActive"+this.mtm.getMaster().getCapFirstDomainName()+""+this.mtm.getSlave().getAliasOrNamePlural()+"Using"+this.mtm.getMaster().getCapFirstDomainName()+"Id(this."+this.mtm.getMaster().getLowerFirstDomainName()+"Id);"));
		sList.add(new Statement(62000L,0,"},"));
		sList.add(new Statement(63000L,0,"methods: {"));
		StatementList sl = mla.generateControllerMethod().getMethodStatementList();
		sl.setSerial(64000L);
		sList.add(sl);		

		sList.add(new Statement(71000L,1,"listAllActive"+this.mtm.getSlave().getAliasOrNamePlural()+"() {"));
		sList.add(new Statement(72000L,1,"listActive"+this.mtm.getSlave().getCapFirstPlural()+"().then("));
		sList.add(new Statement(73000L,2,"response => {"));
		sList.add(new Statement(74000L,2,"this.activeAll"+this.mtm.getSlave().getAliasOrNamePlural()+" = response.data.rows"));
		sList.add(new Statement(75000L,1,"})"));
		sList.add(new Statement(76000L,1,"},"));
		
		sl = lma.generateControllerMethod().getMethodStatementList();
		sl.setSerial(77000L);
		sList.add(sl);
		
		sl = lmaa.generateControllerMethod().getMethodStatementList();
		sl.setSerial(78000L);
		sList.add(sl);	
		
		sl = assign.generateControllerMethod().getMethodStatementList();
		sl.setSerial(79000L);
		sList.add(sl);	
		
		sl = revoke.generateControllerMethod().getMethodStatementList();
		sl.setSerial(80000L);
		sList.add(sl);	

		sList.add(new Statement(99000L,1,"handleChange(value, direction, movedKeys) {"));
		sList.add(new Statement(100000L,2,"console.log(value, direction, movedKeys);"));
		sList.add(new Statement(101000L,2,"//可以通过direction回调right/left 来进行操作，right：把数字移到右边，left把数据移到左边"));
		sList.add(new Statement(102000L,2,"if(direction === \"right\") {"));
		sList.add(new Statement(103000L,3,"this.revoke"+this.mtm.getSlave().getAliasOrNamePlural()+"From"+this.mtm.getMaster().getCapFirstDomainName()+"(this."+this.mtm.getMaster().getLowerFirstDomainName()+"Id,movedKeys.join(\",\"))"));
		sList.add(new Statement(104000L,2,"}"));
		sList.add(new Statement(105000L,2,"if(direction === \"left\") {"));
		sList.add(new Statement(106000L,3,"this.assign"+this.mtm.getSlave().getAliasOrNamePlural()+"To"+this.mtm.getMaster().getCapFirstDomainName()+"(this."+this.mtm.getMaster().getLowerFirstDomainName()+"Id,movedKeys.join(\",\"))"));
		sList.add(new Statement(107000L,2,"}"));
		sList.add(new Statement(108000L,1,"},"));
		
		sl = UtilsMethodGenerator.generateSleepMethodStatements();
		sl.setSerial(109000L);
		sList.add(sl);	

		sList.add(new Statement(115000L,0,"}"));
		sList.add(new Statement(116000L,0,"}"));
		sList.add(new Statement(117000L,0,"</script>"));
		return WriteableUtil.merge(sList);
	}
}
