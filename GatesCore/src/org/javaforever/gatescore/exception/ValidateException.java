package org.javaforever.gatescore.exception;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.ValidateInfo;
import org.javaforever.gatescore.utils.StringUtil;

public class ValidateException extends Exception{
	private static final long serialVersionUID = 2506542517173893138L;
	protected ValidateInfo validateInfo = new ValidateInfo();

	public ValidateInfo getValidateInfo() {
		return validateInfo;
	}

	public void setValidateInfo(ValidateInfo validateInfo) {
		this.validateInfo = validateInfo;
	}
	
	public ValidateException(ValidateInfo info){
		super();
		this.validateInfo = info;
	}
	
	public ValidateException(String info){
		super();
		ValidateInfo validateInfo = new ValidateInfo();
		List<String> msgs = new ArrayList<String>();
		msgs.add(info);
		validateInfo.setCompileErrors(msgs);
		this.validateInfo = validateInfo;
	}
	
	public ValidateException(String info,String type){
		super();
		ValidateInfo validateInfo = new ValidateInfo();
		if (StringUtil.isBlank(type)|| type.equalsIgnoreCase("error")){
			List<String> msgs = new ArrayList<String>();
			msgs.add(info);
			validateInfo.setCompileErrors(msgs);
			this.validateInfo = validateInfo;
		}else if (type.equalsIgnoreCase("warning")){
			List<String> msgs = new ArrayList<String>();
			msgs.add(info);
			validateInfo.setCompileWarnings(msgs);
			this.validateInfo = validateInfo;
		}
		
	}
}
