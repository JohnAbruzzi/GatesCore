package org.javaforever.gatescore.complexverb;

import java.util.ArrayList;
import java.util.List;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontManyToMany;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.Statement;
import org.javaforever.gatescore.core.Writeable;
import org.javaforever.gatescore.core.WriteableUtil;

public class ListMyAvailableActive extends FrontTwinsVerb{
	private static final long serialVersionUID = -7160283265794658162L;
	
	public ListMyAvailableActive(FrontManyToMany mtm) {
		super();
		this.master = mtm.getMaster();
		this.slave = mtm.getSlave();
		this.verbName = "ListAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id";
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id");
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,1,"listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id("+this.master.getLowerFirstDomainName()+"Id) {"));
		sList.add(new Statement(2000L,1,"listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id("+this.master.getLowerFirstDomainName()+"Id).then("));
		sList.add(new Statement(3000L,2,"response => {"));
		sList.add(new Statement(4000L,2,"this.availableActive"+this.slave.getAliasOrNamePlural()+" = [];"));
		sList.add(new Statement(5000L,2,"for(var i=0;i<response.data.rows.length;i++){"));
		sList.add(new Statement(6000L,3,"this.availableActive"+this.slave.getAliasOrNamePlural()+".push(response.data.rows[i].id);"));
		sList.add(new Statement(7000L,2,"}"));
		sList.add(new Statement(8000L,2,"console.log(\"JerryDetect:\"+this.availableActive"+this.slave.getAliasOrNamePlural()+");"));
		sList.add(new Statement(9000L,1,"})"));
		sList.add(new Statement(10000L,1,"},"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;

	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		FrontMethod method = new FrontMethod();
		method.setStandardName("listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id");
		List<Writeable> sList = new ArrayList<Writeable>();
		sList.add(new Statement(1000L,0,"export function listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id("+this.master.getLowerFirstDomainName()+"Id){"));
		sList.add(new Statement(2000L,0,"return request({"));
		if (this.master.isUseController()) {
			sList.add(new Statement(3000L,1,"url:'"+this.master.getLowerFirstDomainName()+"Controller/listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id',"));
		} else {
			sList.add(new Statement(3000L,1,"url:'"+this.master.getLowerFirstDomainName()+"Facade/listAvailableActive"+this.master.getCapFirstDomainName()+""+this.slave.getAliasOrNamePlural()+"Using"+this.master.getCapFirstDomainName()+"Id',"));
		}
		sList.add(new Statement(4000L,1,"method:'post',"));
		sList.add(new Statement(5000L,1,"params:{"+this.master.getLowerFirstDomainName()+"Id:"+this.master.getLowerFirstDomainName()+"Id}"));
		sList.add(new Statement(6000L,0,"})"));
		sList.add(new Statement(7000L,0,"}"));
		method.setMethodStatementList(WriteableUtil.merge(sList));
		return method;

	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
